<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'agendaflou_description' => 'Ce plugin ajoute un champ pour chaque (début et fin) d’un événement afin de pouvoir spécifier qu’elles ne sont pas forcément précises. Une liste permet alors de spécifier le niveau de précision. On peut alors dire qu’une date n’est précise qu’au mois, à la saisie ou à l’année.',
	'agendaflou_nom' => 'Agenda : dates floues',
	'agendaflou_slogan' => 'Spécifier que certaines dates ne sont pas précises',
);

