<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'date_floue_choix_annee' => 'Année',
	'date_floue_choix_mois' => 'Mois',
	'date_floue_choix_normale' => 'Normale',
	'date_floue_choix_saison' => 'Saison',
	'date_floue_debut_label' => 'Précision de la date de début',
	'date_floue_fin_label' => 'Précision de la date de fin',
	'date_floue_label' => 'Dates floues',
	'date_floue_utiliser' => 'Utiliser les dates floues',
);

